<?php

namespace PhangoApp\PhaRouter2;

class Router {
    
    
    static public $base_path='';

    static public $root_url='/';
    
    static public $apps=[];
    
    static public $app=[];    
    
    static public $root_path='';
    
    static public $path_theme=['views/default'];

    static public $path_media=['media'];
    
    static public $base_file='index.php';
    
    static public $routes=[];
    
    static public $DEBUG=true;
    
    public function response($path_info) 
    {
        $func_route='';
        
        $args=[];
        
        $z=false;
        
        if(!$z && $path_info=='')
        {
            
            list($namespace_route, $func_route, $file_route)=Router::$app;
            
        }
        else
        {
            foreach(Router::$routes as $k_route => $route)
            {
                
                $match=str_replace('/', '\/', $k_route);

                if(preg_match('/^'.$match.'$/', $path_info, $args))
                {
                    
                    list($namespace_route, $func_route, $file_route)=$route;
                    $z=true;
                    
                    break;
                    
                }
                
            }
        
        }
        
        if($func_route!='')
        {
        
            if(is_file($file_route))
            {
                
                include($file_route);
                
                $controller=new $namespace_route(str_replace('controllers', '', dirname($file_route)));
                
                array_shift($args);
                
                return call_user_func_array([$controller, $func_route] , $args );
                
            }
        
        }
        else
        {
            
            return $this->response404();
            
        }
    
    }
    
    public function response404()
	{
	
		header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
		
		/*$url404=$this->make_url($this->default_404['controller'], $this->default_404['method'], $this->default_404['values']);
		
		//Use views for this thing.
		
		if(!$this->response($url404, 0))
		{*/
		
		//use a view
		
        return 'Error: page not found...';
			
		//}
		
		
		//$this->response($url404);
	}
    
    /**
    * method used for create a valid url for phango
    * 
    * @param string $url_path The path of url without Router::$root_url;
    * @param array $values
    */
    
    static public function make_url($url_path, array $values=array(), array $get=array())
	{
        $url=Router::$root_url.Router::$base_file.'/'.$url_path.'/'.implode('/', $values);
        
        return Router::add_get_parameters($url, $get);
    }
	
	/**
	* Function used for add get parameters to a well-formed url based on make_fancy_url, make_direct_url and others.
	*
	* @param string $url_fancy well-formed url
	* @param string $arr_data Hash with format key => value. The result is $_GET['key']=value
	*/

	static public function add_get_parameters(string $url_fancy, array $arr_data)
	{

		$arr_get=array();
		
		$sep='';
		
		$get_final='';
		
		if(count($arr_data)>0)
		{

			foreach($arr_data as $key => $value)
			{

				$arr_get[]=$key.'/'.$value;

			}

			$get_final=implode('/', $arr_get);

			$sep='/get/';

			if(preg_match('/\/$/', $url_fancy))
			{

				$sep='get/';

			}
			
			
			if(preg_match('/\/get\//', $url_fancy))
			{

				$sep='/';

			}
			
		}

		return $url_fancy.$sep.$get_final;

	}

    static public function make_media_url(string $file_path, $module=false)
	{
        if(!$module) 
        {
        
            $url=Router::$root_url.Router::$path_media.'/'.$file_path;
            
        }
        else
        {
            
            $url=Router::$root_url.'vendor/'.$module.'/media/'.$file_path;
            
        }
        
        return $url;
    }

	
	/**
	* Method for make simple redirecs using header function.
	* @param string $url The url to redirect
	*/
	
	static public function redirect($url)
	{
	
		header('Location: '.$url);
	
		die;
	
	}

    
}
