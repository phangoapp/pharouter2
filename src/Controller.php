<?php

namespace PhangoApp\PhaRouter2;

use PhangoApp\PhaI18n\I18n;

class Controller {
    
    public $twig;
    
    public function __construct($file_path) {
        //Create twig template
        
        if(is_dir($file_path.'/views'))
        {
        
            Router::$path_theme[]=$file_path.'/views';
            
        }
        
        $loader = new \Twig_Loader_Filesystem(Router::$path_theme);
        
        $this->twig = new \Twig_Environment($loader, ['cache' => 'views/cache', 'auto_reload' => Router::$DEBUG]);

        $make_url=new \Twig_Function('make_url', function ($string, $values=[], $extra_values=[]) {
            return Router::make_url($string, $values, $extra_values);
        });

        $make_media_url=new \Twig_Function('make_media_url', function ($file_path, $module=false) {
            return Router::make_media_url($file_path, $module);
        });

        $lang=new \Twig_Function('lang', function ($app, $code_lang, $default_lang) {
            return I18n::lang($app, $code_lang, $default_lang);
        });
        
        $this->twig->addFunction($make_url);
        $this->twig->addFunction($make_media_url);
        $this->twig->addFunction($lang);
        
    }

    public function session() 
    {
        
        session_name(COOKIE_SESSION_NAME.'_session');

        session_set_cookie_params(0, Router::$root_url);

        session_start();
        
    }

    public function response_json($json_to_encode)
    {
        
        header('Content-Type: application/json');
        
        return json_encode($json_to_encode);
        
    }

}
