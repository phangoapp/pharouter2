<?php

namespace PhangoApp\Welcome;

use PhangoApp\PhaRouter2\Controller;

class PageController extends Controller {
    
    public function __construct($file_path) 
    {
        
        parent::__construct($file_path);
        $this->session();
        
    }

    /*
    * @PhangoController /welcome PhangoApp\Welcome\PageController\Home
    */

    public function home()
    {
        
        return $this->twig->render('home.html', ['var' => 'prueba']);

    }

    //A example with symlink

    //The types are string, integer, float. 

    /*
    * @PhangoController /welcome/([0-9]+)/([a-zA-Z0-9_\-]+) PhangoApp\Welcome\PageController\Page
    */
        
    public function page($number, $str)
    {
            
        return 'This the number '.$number.'This is the string '.$str;
            
    }
    
    //A example for a very simple json api

    /*
    * @PhangoController /api PhangoApp\Welcome\PageController\Json
    */
    
    public function json() 
    {
        
        return  $this->response_json(['user' => 'anonymous']);
            
    }

}

?>
